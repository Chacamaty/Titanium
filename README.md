Titanium
==

[![build status](https://gitlab.com/hbomb79/Titanium/badges/develop/build.svg)](https://gitlab.com/hbomb79/Titanium/commits/develop)
[![Language (Lua)](https://img.shields.io/badge/powered_by-Lua-blue.svg?style=flat)](https://lua.org)
[![License (MIT)](https://img.shields.io/badge/license-MIT-blue.svg?style=flat)](http://opensource.org/licenses/MIT)
[![Platform (CC)](https://img.shields.io/badge/platform-ComputerCraft-blue.svg?style=flat)](http://computercraft.info)
[![Gitter](https://badges.gitter.im/hbomb79/Titanium.svg)](https://gitter.im/hbomb79/Titanium?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)


#### About
Titanium is a feature-rich, reliable framework for creating GUIs within ComputerCraft. Titanium offers a quick and easy experience for those that just want to jump in, however more powerful tools are on standby if you need them.

![](http://puu.sh/rd2ty/c8cc92aa93.gif)

#### Alpha
Titanium is currently in alpha and your feedback, bug reports, suggestions (via [issues](https://gitlab.com/hbomb79/Titanium/issues)) and [merge requests](https://gitlab.com/hbomb79/Titanium/merge_requests) are welcomed.

#### Download and Installation
Information regarding the multiple methods to download Titanium can be found at the [titanium website](http://harryfelton.web44.net/titanium/).

#### Documentation
The [documentation website](http://harryfelton.web44.net/titanium/doc) automatically updates to provide information regarding the latest release of Titanium (ability to view older documentation is provided).
